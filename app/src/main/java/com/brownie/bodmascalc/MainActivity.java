package com.brownie.bodmascalc;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = "MainActivity";

    private BodmasCalculator bodmasCalculator;
    private EvaluateString evaluateString;
    private EditText textArea;
    private String text;
    private TextView errorMessageText;

    private String result;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        textArea = findViewById(R.id.edit_text);
        errorMessageText = findViewById(R.id.error_message);
        errorMessageText.setVisibility(View.GONE);

    }

    public void getTextFromButton(View view) {

        errorMessageText.setVisibility(View.GONE);
        String buttonText = (String) ((Button)view).getText();

        text = textArea.getText().toString();
        text = text+buttonText;
        textArea.setText(text);
    }

    public void evaluateExpression(View view) {
/*
        bodmasCalculator = new BodmasCalculator();
        text = textArea.getText().toString();

        String postfixExpression = bodmasCalculator.findPostfix(text);

        if(postfixExpression.equals("Invalid expression")) {
            errorMessageText.setVisibility(View.VISIBLE);
            textArea.setText("");
            return;
        }

        double result = bodmasCalculator.evaluatePostfix(postfixExpression);

        if(result%1==0)
        {
            textArea.setText("" + (int)result);
        }
        else
        {
            textArea.setText(""+result);
        }
        */

        evaluateString = new EvaluateString();

        text = textArea.getText().toString();

/*        if(result != null)
        {
            text = text + result;
        }*/

        double postfixExpression = EvaluateString.evaluate(text);

        if(postfixExpression == 9.818029)
        {
            errorMessageText.setVisibility(View.VISIBLE);
            textArea.setText("");
            result=null;
            return;
        }

/*        if(postfixExpression.equals("Invalid expression")) {
            errorMessageText.setVisibility(View.VISIBLE);
            textArea.setText("");
            return;
        }*/

//        double result = bodmasCalculator.evaluatePostfix(postfixExpression);

        if(postfixExpression % 1 == 0)
        {
            result = String.valueOf((int)postfixExpression);
            //textArea.setText("" + (int)postfixExpression);
        }
        else
        {
            result = String.valueOf(postfixExpression);
//            textArea.setText(""+result);
        }

        textArea.setText(result);
    }

    public void clearTextField(View view) {
        textArea.setText("");
        result = null;
    }

    public void findSqrt(View view) {
        text = textArea.getText().toString();

        /*if(result!=null)
        {
            text = text + result;
        }*/

        double value = 0;

        try {
            value = Double.parseDouble(text);
            textArea.setText(""+Math.sqrt(value));
            errorMessageText.setVisibility(View.GONE);
        }
        catch (NumberFormatException e) {
            errorMessageText.setVisibility(View.VISIBLE);
            textArea.setText("");
        }
    }

    public void findLog(View view) {
        text = textArea.getText().toString();
        double value = 0;
        try {
            value = Double.parseDouble(text);
            textArea.setText(""+Math.log10(value));
            errorMessageText.setVisibility(View.GONE);
        }
        catch (NumberFormatException e) {
            errorMessageText.setVisibility(View.VISIBLE);
            textArea.setText("");
        }
    }

}
