package com.brownie.bodmascalc;

import java.util.ArrayList;
import java.util.Scanner;
import java.util.Stack;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


class BodmasCalculator
{
    ArrayList<Character> operators;

    BodmasCalculator()
    {
        operators=new ArrayList<>();
        operators.add('^');
        operators.add('/');
        operators.add('*');
        operators.add('+');
        operators.add('-');
    }
    
    
    String findPostfix(String inputP)
    {
        if(!validateInput(inputP))
            return "Invalid expression";
        String temp="";
        ArrayList<Character> al=new ArrayList<>();
        al.add('(');
        char ch;
        inputP=inputP+")";
        for(int i=0;i<inputP.length();i++)
        {
            ch=inputP.charAt(i);
           if(ch=='(')
               al.add('(');
           else if(operators.contains(ch))
           {
               al.add(ch);
               temp=temp+pop(al,ch);
           }    
           else if(ch==')')
               temp=temp+pop(al);
           else
           {
               while(ch!='(' && ch!=')' && !operators.contains(ch))
               {
                   temp=temp+ch;
                   i=i+1;
                   ch=inputP.charAt(i);
               }
               temp=temp+" ";
               i=i-1;
           }
           
        }
        return temp;
    }
    
    
    private String pop(ArrayList<Character> al, char ch)
    {
        String temp="";
        for (int i = al.size()-1;  al.get(i)!='(' && i>=0;   i--) 
        {
            if(operators.indexOf(al.get(i))<operators.indexOf(ch))
            {
                temp=temp+al.get(i);
                al.remove(i);
            }    
        }
        return temp;
    }

    private boolean validateInput(String input) {
        if((input.replaceAll("[^(]","" ).length() != input.replaceAll("[^)]", "").length()) || input.split("[^\\d]").length == 0 || input.length() == 0)
            return false;

            Pattern pattern = Pattern.compile("(\\d\\()|(\\)\\d)|(\\([*\\/+^-])|([*\\/+^-]\\))");
            Matcher matcher = pattern.matcher(input);
            if (matcher.find())
                return false;

        pattern = pattern.compile("[^\\d|+*^()\\/-]");
        matcher = pattern.matcher(input);
        if (matcher.find())
            return false;


        pattern = pattern.compile("[*-+^\\/]{2}");
        matcher = pattern.matcher(input);
        if (matcher.find())
            return false;

        return true;
    }
    
    
    private String pop(ArrayList<Character> al)
    {
        String temp="";
        for (int i = al.size()-1;  al.get(i)!='(' && i>=0;   i--) 
        {
                temp=temp+al.get(i);
                al.remove(i);
        }
        al.remove(al.size()-1);
        return temp;
    }
    
    
    float evaluatePostfix(String inputP)
    {
        Stack<Float> stack=new Stack<>();
        char ch;
        float inputA;
        float inputB;
        for (int i = 0; i < inputP.length(); i++)
        {
            ch=inputP.charAt(i);
            
            if(operators.contains(ch))
            {
                inputB=stack.pop();
                inputA=stack.pop();
                switch(ch)
                {
                    case '^' :
                        stack.push((float)Math.pow(inputA,inputB));
                        break;
                    case '/' :
                        stack.push(inputA/inputB);
                        break;
                    case '*' :
                        stack.push(inputA*inputB);
                        break;
                    case '+' :
                        stack.push(inputA+inputB);
                        break;
                    case '-' :
                        stack.push(inputA-inputB);
                        break;
                }
            }
            
            
            else
            {
                String temp="";
                while(!operators.contains(ch) && i<inputP.length() && ch!=' ')
                {
                    temp=temp+ch;
                    i=i+1;
                    ch=inputP.charAt(i);
                }
                stack.push((Float.parseFloat(temp)));
            }
                
        }
        return stack.pop();
    }

}
